/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author HP
 */
public class Hashing {
    public static void main(String[] args) {
        Node node = new Node();
        System.out.println(node.get(0)); // null

        // เพิ่ม key = 1 , value = JAVA 
        node.put(1, "JAVA");
        System.out.println(node.get(1)); 
        System.out.println(node.get(2));

        // ลบ key ที่ 1
        node.remove(1);
        System.out.println(node.get(1)); 
    }

}
